# Semantic Data Discovery

DARE Semantic Data Discovery Service is an application to search through linked data. 

## Install instructions
Before installation and use of this application you have to install [Solr](https://lucene.apache.org/solr/guide/8_3/index.html)
and at least one endpoint like [Virtuoso](https://vos.openlinksw.com/owiki/wiki/VOS/) or [Semagrow](https://github.com/semagrow/semagrow) before.

This application will configure Solr fields for the most part on it's own at startup. Beside this it is mandatory to 
whether remove field `geometry` from ./config/mapping.xml to skip spatial support or configurate Solr as stated 
in the Solr Spatial Configuration [JTS and Polygons](https://lucene.apache.org/solr/guide/8_3/spatial-search.html#jts-and-polygons-flat).
Make sure you use the proper Solr documentation version.

1. Clone this repository to your computer. 
```shell script
git clone https://gitlab.scai.fraunhofer.de/fabian.wolf/semantic-data-discovery.git /var/semantic-data-discovery
```
2. Go to the root folder of this repository 
```shell script
cd /var/semantic-data-discovery
```
3. Install dependencies 
```shell script
pip install -r ./requirements.txt
```
4. Configurate application. Visit [Configuration](#configuration) for more informations.
```shell script
cp ./config/config_example.json ./config/config.json
nano ./config/config.json
```
5. Run application. For testing, it is enough to execute `main.py`: 
``` shell script
python main.py
```
For production purposes use a WSGI server, for example [Waitress](https://docs.pylonsproject.org/projects/waitress/en/stable/).

To install waitress use pip:
```shell script
pip install waitress
```
To run this application call: 
```shell script
waitress-serve --port 5000 --call app.ui:create_app
```

## Configuration
You can find an example configuration at `./config/config_example.json`. It's described as follows:
### App
Application configuration.
* **name**: Name of this application
* **description**: Description of this application
* **mapping_file**: Path to one or more mapping file. If you want to include more than one file, use a python list instead of a string. This application contains a default configuration at `./config/mapping.xml`. For more informations about writing own mapping file visit the [Mapping Documentation](./doc/mapping.md)
* **log_file**: Path to the application logfile. The file will automatically created at first start.
* **log_level**: The possible Log-Levels are `CRITICAL`, `ERROR`, `WARNING`, `INFO` and `DEBUG` in decreasing order.
* **search_preview**: A list of solr fields, which are included to the search response.
These can be overwritten in the search request. Default is `id`, `endpoint`, `dct_identifier`.
### WEB
Webserver configurations.
* **host**: Hostname on which this service is avaiable.
* **port**: Port on which this service is avaiable.
### Solr
Configuarations concerning the Search and index engine Solr.
* **host**: URL on which solr is accessible.
* **core**: Solr core which is used to index and query every found data.
* **temp_path**: Path with write access for temporary solr documents for index purpose.
### Endpoints
Endpoints are the source for this data discovery. They are configured as json objects with in the endpoint namespace.
* **name**: Uniqe name for this Endpoint. It is used for example to address the index documents for this endpoint.
* **type**: There are two types of endpoints possible:
    
    `graph` A single file accessible on this PC/Docker or via Web containing RDF Triples.
    * **file**: Path or URL to the file.
    * **rdf_format**: Format of the RDF file. Possible formats are: `json-ld`, `turtle`, `n3`, `xml` and some more see [Parser](https://rdflib.readthedocs.io/en/stable/plugin_parsers.html)
    * **replace_bnodes**: Should only be used for testing purposes. The Ids produced here are not stable and change every startup.
    
    `url` SPARQL Endpoint
    * **url**: Path to the SPARQL Endpoint.

Full example:
``` json
{
  "app": {
    "name": "Semantic Data Discovery",
    "description": "Service to index and discover semantic data in DARE",
    "log_file": "../data/app.log",
    "log_level": "DEBUG",
    "search_preview": [
          "endpoint",
          "dct_identifier",
          "dct_title",
          "content"]
  },
  "web": {
    "host": "localhost",
    "port": "8080"
  },
  "solr": {
    "host": "http://localhost:8983/solr/",
    "core": "test",
    "temp_path": "../data/cache/"
  },
  "endpoints": [
    {"name": "Virtuoso", "type":  "url", "url":  "http://localhost8890/sparql"},
    {"name":  "Opendata", "type": "graph", "file":  "~/Documents/data.json", "rdf_format": "json-ld", "replace_bnodes": true}
  ]
}
```
### CLI Configurator
There is a configuration tool for a dynamic configuration of the web service. It's especially helpful for automated 
environments like docker or other container tools.

Usage: 
```shell script
configurate.py --help
> configurate.py endpoint -n <name> -t [url -u <url>|graph -f <file>]
> or
> configurate.py solr -u <host> -c <core>
``` 
Example:
```shell script
configurate.py endpoint -n virtuoso -t url -u "http://localhost:8890/sparql"
configurate.py solr -u "http://localhost:8983/solr/" -c "core"
```
## Usage
Semantic Data Discovery offers three key resources to control this application.

* `/index/...` Manage search index with view, create and delete operations
    * `/` shows (GET), creates/updates (PUT) and  deletes (DELETE) all indexed datasets.
    * `endpoints/` Shows configured endpoints.
    * `endpoints/{name}/` shows (GET), creates/updates (PUT) and  deletes (DELETE) all indexed datasets from this endpoint.
    Single dataset only is possible.
* `/search/` Search in indexed datasets.
Offers a full-text search query and filter fields to search in the index.
* `/monitoring/...` Monitor application
    * `status/` Checks the connection to the external services solr and endpoints.
    * `heartbeat/` Offers an easy-to-call resource to test the connection to Semantic Data Discovery.

You can find a full API documentation on [http://localhost:5000/api/](http://localhost:5000/api/).
By using the "Try it out" button within the listed operations you can even call the resources. 

## Requirements
Install with: `pip install -r ./requirements.txt`
* werkzeug
* flask
* flask_restplus
* SPARQLWrapper
* rdflib
* SolrClient
* rdflib-jsonld
Tested with Python 3.7