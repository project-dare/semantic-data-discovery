# Mapping Documentation
To have a use of the countless semantic vocabularies available and especially the ones which fits your needs,
there is an easy way to make Semantic Data Discovery recognize and use them.
You can define new vocabularies with XML. 
You can either use already existing mapping files or create new by adding them to the `config.json`.

## Structure
All vocabularies in a mapping file are wrapped by a `<vocabularies>`-Tag.
Within this tag you can have multiple `<vocabulary>` elements.
There you should link the URI `<uri>` and name a prefix `<prefix>` of the desired vocabulary.
Both of them must be unique.
Then within an `<fields>` element you have to list all properties which should be indexed from this vocabularie.
There are 3 attributes in the `<field>` element listed below:
## Field
* `name` of the RDF property.
* `type` SOLR type see description [below](#types).
* `custom` Path to a python function see description [below](#custom).

Within the `<field>` element you can list SOLR field properties with `<property name= "" value=""/>`.
They are discribed in the official SOLR 
[documentary](http://lucene.apache.org/solr/guide/8_3/field-type-definitions-and-properties.html#field-default-properties)
and overwrites the type properties.

Example:
```xml
<vocabularies>
    <vocabulary>
        <prefix>dct</prefix>
        <uri>http://purl.org/dc/terms/</uri>
        <fields>
            <field name="title" type="TEXT"/>
            <field name="description" type="TEXT"/>
            <field name="format" type="TEXT"/>
            <field name="identifier" type="TEXT" custom="app.logic.mapper.vocabularies.dct.identifier"/>
            <field name="issued" type="DATE" />
            <field name="modified" type="DATE" />
            <field name="spatial" type="CONTAINER" custom="app.logic.mapper.vocabularies.dct.spatial"/>
            <field name="demo" type="TEXT">
                <property name="Property1" value="true"/>
                <property name="Property2" value="true"/>
            </field>
        </fields>
    </vocabulary>
   <!-- ... -->
</vocabularies>
```

## Types
* **CONTAINER**:
This is not really a solr field type but a way to structure your solr index with sub documents.
Typically you link RDF nodes like DCAT Distribution to this type.
* ~~ROOT~~ _not supported, yet_:
This is an root element in future you could inheritances from DCAT Dataset with it.
But currently only this one is supported.
* **TEXT**:
Gets automatically copied to the full-text search.
* **BOOLEAN**:
* **DATE**:
Format: YYYY-MM-DDThh:mm:ssZ 
See also: [SOLR documantation](http://lucene.apache.org/solr/guide/8_3/working-with-dates.html#date-formatting)
* **FLOAT**:
* **INT**:
* **LONG**:
* **LOCATION_POINT/LOCATION**:
Format: GeoJSON and WKT. 
See also: [SOLR documantation](http://lucene.apache.org/solr/guide/8_3/spatial-search.html#indexing-points)
* **LOCATION_POLYGON**:
Format: GeoJSON and WKT. 
See also: [SOLR documantation](http://lucene.apache.org/solr/guide/8_3/spatial-search.html#indexing-points)

If you receive other formats in the RDF literals, you have to use the custom attribute with a python function to convert this field to the needed format.

## Custom
The custom functions use the following structure: `def function(self, request, type, res):`
Attributes:
* **self**: Reference to the processing object of type `Vocabulary`.
You can call self._discover(request, res) to discover this node further in case of a CONTAINER.
* **request**: Object which contains information of the received property.
Like the Property URI in `predicate` or the literal in case of an literal in `object`.
* **type**: RDF type of this node ('bnode', 'uri', 'literal', 'typed-literal').
* **res**: Reference to the return value as dict.
Alternatively the return can be used for simple return values like strings.

Example: 
```python
def spatial(self, request, type, res):
    if type == "uri" or type == "bnode":
        self._discover(request, res, verbose=True)
    if type == "literal":
        from app.logic.schema.fields import fields
        if "locn_geometry" in fields:
            res["locn_geometry"] = request.object
```

```python
class Request:

    def __init__(self, predicate, obj, endpoint, path=None, literal_type=None):
        """
        :param predicate: string
        :param obj: string
        :param endpoint: app.database.sparql.connector.Endpoint
        """
        self.predicate = predicate
        self.object = obj
        self.endpoint = endpoint
        self.path = path
        self.literal_type = literal_type
```