import json
from unittest import TestCase, mock

from app.database.solr import index
from app.database.sparql import endpoint
from app.database.sparql.connector import RootPath
from app.database.sparql.endpoint import get_endpoint
from app.database.sparql.query_builder import SelectQuery, QElement
from app.logic.config import config


def get_uri_by_identifier(endpoint, identifier):
    """
    :param endpoint: Endpoint
    :param identifier:
    :return: str
    """
    query = SelectQuery()
    query.add_prefix("dcat", "http://www.w3.org/ns/dcat#")
    query.add_prefix("dc", "http://purl.org/dc/terms/")
    query.add_element(
        QElement(data='?id', statement='?id dc:identifier \'{id}\''.format(id=identifier), group_by=True,
                 optional=False))

    result_set = endpoint.query(query.build())

    for result in result_set:
        return u"nodeID://{id}".format(id=result["id"]["value"])
    return ""


def get_uri_by_title(endpoint, title):
    """
    :param endpoint: Endpoint
    :param title: str
    :return: str
    """
    query = SelectQuery()
    query.add_prefix("dcat", "http://www.w3.org/ns/dcat#")
    query.add_prefix("dc", "http://purl.org/dc/terms/")
    query.add_element(
        QElement(data='?id', statement='?id dc:title \'{title}\''.format(title=title), group_by=True,
                 optional=False))

    result_set = endpoint.query(query.build())

    for result in result_set:
        return u"nodeID://{id}".format(id=result["id"]["value"])
    return ""


def construct(endpoint, dataset):
    ret = {
        'dcat_keyword': dataset["keyword"],
        'id': get_uri_by_identifier(get_endpoint(endpoint), dataset["identifier"]),
        'type': "root",
        'endpoint': 'test1',
        'dcat_distribution': []
    }
    for distribution in dataset["distribution"]:
        dist = {
            'id': get_uri_by_title(get_endpoint(endpoint), distribution["title"]),
            'type': 'dcat_distribution',
            'dcat_mediatype': distribution["mediaType"]
        }
        ret["dcat_distribution"].append(dist)

    if type(ret["dcat_keyword"]) is str:
        ret["dcat_keyword"] = [ret["dcat_keyword"]]

    if len(ret["dcat_distribution"]) == 1:
        ret["dcat_distribution"] = ret["dcat_distribution"][0]

    return ret


class Test(TestCase):
    datasource = {}
    calls = {}

    def setUp(self) -> None:
        from app.database.solr.schema import creation_worker
        creation_worker.add_field = mock.MagicMock(return_value=None)

        path = "./dcat_test.json"
        config.conf["endpoints"] = []
        config.conf["endpoints"].append(
            {"name": "test1", "type": "graph", "rdf_format": "json-ld", "file": path}
        )
        config.conf["endpoints"].append(
            {"name": "test2", "type": "graph", "rdf_format": "json-ld", "file": path}
        )
        config.conf["solr"] = {"host": "", "core": "", "temp_path": "./data/temp/"}
        config.conf["app"] = {"mapping_file": []}
        with open(path) as file:
            self.datasource = json.load(file)

            for dataset in self.datasource["dataset"]:
                one = construct("test1", dataset)
                two = construct("test2", dataset)

                self.calls[one["id"]] = one
                self.calls[two["id"]] = two

        self.maxDiff = None
        index.add = mock.MagicMock()
        index.commit = mock.MagicMock()
        index.remove = mock.MagicMock()
        index.show = mock.MagicMock()

    def tearDown(self) -> None:
        self.datasource = {}
        self.calls = {}
        endpoint.cache = {}

    def assertCallEquals(self, call):
        b = call[0][0]
        a = self.calls[b["id"]]
        a = {key: a[key] for key in sorted(a.keys())}
        b = {key: b[key] for key in sorted(b.keys())}
        # same source
        if "endpoint" in a:
            del a["endpoint"]
        if "endpoint" in b:
            del b["endpoint"]

        # sort everything for an equal dict...
        if "dcat_keyword" in a and type(a["dcat_keyword"]) is list:
            a["dcat_keyword"].sort()
        if "dcat_keyword" in b and type(b["dcat_keyword"]) is list:
            b["dcat_keyword"].sort()

        # solr doesn't care if it's a 1 elem list or a str
        if "dcat_keyword" in a and "dcat_keyword" in b:
            if type(a["dcat_keyword"]) == str and len(b["dcat_keyword"]) == 1:
                a["dcat_keyword"] = [a["dcat_keyword"]]

            if type(b["dcat_keyword"]) == str and len(a["dcat_keyword"]) == 1:
                b["dcat_keyword"] = [b["dcat_keyword"]]

        if "dcat_distribution" in a:
            if type(a["dcat_distribution"]) is list:
                a["dcat_distribution"].sort(key=lambda x: x['id'])
                for elem in a["dcat_distribution"]:
                    sorted(elem.items(), key=lambda x: x[1])
            else:
                sorted(a["dcat_distribution"].items(), key=lambda x: x[1])

        if "dcat_distribution" in b:
            if type(b["dcat_distribution"]) is list:
                b["dcat_distribution"].sort(key=lambda x: x['id'])
                for elem in b["dcat_distribution"]:
                    sorted(elem.items(), key=lambda x: x[1])
            else:
                sorted(b["dcat_distribution"].items(), key=lambda x: x[1])

        self.assertDictEqual(a, b)

    def test_index_all(self):
        from app.logic.mapper.indexer import index_all
        index_all()
        self.assertEqual(index.add.call_count, 20)
        for call in index.add.call_args_list:
            self.assertCallEquals(call)

        index.commit.assert_called()

    def test_index_endpoint(self):
        from app.logic.mapper.indexer import index_endpoint
        index_endpoint("test1")
        self.assertEqual(10, index.add.call_count)
        for call in index.add.call_args_list:
            self.assertCallEquals(call)

        index.commit.assert_called()

    def test_index_dataset(self):
        from app.logic.mapper.indexer import index_dataset
        ident = "d460252e-d42c-474a-9ea9-5287b1d595f6"
        path = RootPath(ident, "http://purl.org/dc/terms/identifier")
        endpoint = get_endpoint("test1")
        index_dataset(endpoint, get_uri_by_identifier(endpoint, ident), path=path, hard_commit=True)

        index.add.assert_called_once()

        for call in index.add.call_args_list:
            self.assertCallEquals(call)

        index.commit.assert_called()

    def test_index_remove(self):
        from app.logic.mapper.indexer import index_remove
        index_remove("test1", "id")
        index.remove.assert_called_once_with("test1", "id")
        index.commit.assert_called()

    def test_index_show(self):
        from app.logic.mapper.indexer import index_show
        index_show("test1", "id")
        index.show.assert_called_once_with("test1", "id")
