from unittest import TestCase, mock

from app.database.sparql.endpoint import get_endpoint
from app.logic.config import config
from app.logic.search.perform import search
from app.database.solr import query as solr
from test.test_indexer import get_uri_by_identifier


class Test(TestCase):
    def test_search(self):
        path = "./dcat_test.json"
        config.conf["endpoints"] = []
        config.conf["endpoints"].append(
            {"name": "test1", "type": "graph", "rdf_format": "json-ld", "file": path}
        )
        config.conf["app"] = {}
        config.conf["app"]["search_preview"] = ["id", "endpoint", "dct_identifier", "content"]

        id1 = "d460252e-d42c-474a-9ea9-5287b1d595f6"
        id2 = "95f8eac4-fd1f-4b35-8472-5c87e9425dfa"
        res_map = {
            id1: {
                "id": get_uri_by_identifier(get_endpoint("test1"), id1),
                "endpoint": "test1",
                "dct_identifier": id1,
            },
            id2: {
                "id": get_uri_by_identifier(get_endpoint("test1"), id2),
                "endpoint": "test1",
                "dct_identifier": id2,
            }
        }

        ret = type('return', (object,), {})()
        ret.docs = [res_map[id1], res_map[id2]]

        ret.get_num_found = mock.MagicMock(return_value=len(ret.docs))
        solr.query = mock.MagicMock(return_value=ret)

        result = search({"aaa": "bbb"}, "", 0, 10, "turtle")
        solr.query.assert_called_once()

        self.assertEqual(len(ret.docs), result[0])
        for ident in res_map:
            a = res_map[ident]
            b = {}
            for res in result[1]:
                if a["id"] == res["id"]:
                    b = res
            self.assertEqual(a["id"], b["id"])
            self.assertEqual(a["endpoint"], b["endpoint"])
            self.assertEqual(a["dct_identifier"], b["dct_identifier"])
            self.assertNotEqual("", b["content"])
