from unittest import TestCase, mock


class Test(TestCase):
    def test_add_field(self):
        from app.database.solr.schema import creation_worker
        creation_worker.add_field = mock.MagicMock(return_value=None)

        from app.logic.schema import add_field
        from app.logic.schema.fields import FieldType, fields

        name = "field"
        type = FieldType("text_general", "solr.TextField", formatter=str, copy_field="text")
        default = "def",
        properties = {"1": "abc", "2": "cde"}

        add_field(name, type, default, properties)

        creation_worker.add_field.assert_called_with({"name": name, "type": type.name, "default": default,
                                                      "1": properties["1"], "2": properties["2"]}, type.copy)
        self.assertTrue(name in fields)
        field = fields[name]
        self.assertEqual(name, field.name)
        self.assertEqual(type, field.type)
        self.assertEqual(default, field.default)
        self.assertEqual(properties, field.properties)
