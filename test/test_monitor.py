from unittest import TestCase, mock

from app.database import solr
from app.database.sparql.endpoint import get_endpoints
from app.logic.config import config
from app.logic.monitor import ping_all


class Test(TestCase):
    def test_ping_all(self):
        path = "./dcat_test.json"
        config.conf["endpoints"] = []
        config.conf["endpoints"].append(
            {"name": "test1", "type": "graph", "rdf_format": "json-ld", "file": path}
        )
        config.conf["endpoints"].append(
            {"name": "test2", "type": "graph", "rdf_format": "json-ld", "file": path}
        )
        config.conf["solr"] = {"host": "", "core": "", "temp_path": "./data/temp/"}

        solr.ping = mock.MagicMock()
        for endpoint in get_endpoints():
            endpoint.ping = mock.MagicMock()

        ping_all()

        solr.ping.assert_called_once()
        for endpoint in get_endpoints():
            endpoint.ping.assert_called_once()
