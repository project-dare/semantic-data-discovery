from SPARQLWrapper import SPARQLWrapper, JSON
from rdflib import Graph, URIRef, Literal, BNode

from app.database.sparql.query_builder import SelectQuery, QElement
from app.logic.logger import logger


class Binding:
    def __init__(self):
        self.elements = []
        self.name_map = {}

    def __iter__(self):
        return self

    def __next__(self):
        return next(self.elements)

    def append(self, element):
        self.elements.append(element)
        self.name_map[element.name] = element

    def iter(self):
        return iter(self)

    def __getitem__(self, item):
        if item in self.name_map:
            return self.name_map[item]


class Path:
    """
    class to describe path to follow blanknodes in sparql querys
    """

    def __init__(self, start, predicate):
        self.start = start
        self.predicates = [predicate]

    def __copy__(self):
        obj = Path(self.start, self.predicates[0])
        for uri in self.predicates[1:]:
            obj.predicates.append(uri)

        return obj

    def __str__(self):
        return "{start} > {path}".format(start=self.start, path=" > ".join(self.predicates))

    def start_query(self, query_builder, variable):
        query_builder.add_element(QElement(
            data='',
            statement='<{start}> <{predicate}> ?{variable}'.format(start=self.start, predicate=self.predicates[0],
                                                                   variable=variable),
            group_by=False,
            optional=False
        ))

    def add_to_query(self, query_builder):

        if len(self.predicates) > 1:
            i = 0
            self.start_query(query_builder, str(i))

            for uri in self.predicates[1:-1]:
                query_builder.add_element(QElement(
                    data='',
                    statement='?{s} <{predicate}> ?{o}'.format(s=i, predicate=uri, o=i + 1),
                    group_by=False,
                    optional=False
                ))
                i = i + 1

            query_builder.add_element(
                QElement(data='', statement='?{s} <{predicate}> ?object'.format(s=i, predicate=self.predicates[i + 1]),
                         group_by=False, optional=False))
        else:
            self.start_query(query_builder, "object")


class RootPath(Path):
    def __init__(self, identifier, predicate):
        super().__init__(identifier, predicate)

    def __str__(self):
        return "{predicate}:{id} > {path}".format(id=self.start, predicate=self.predicates[0],
                                                  path=" > ".join(self.predicates[1:]))

    def start_query(self, query_builder, variable):
        query_builder.add_element(QElement(
            data='',
            statement='?{variable} <{predicate}> ?id'.format(variable=variable, predicate=self.predicates[0]),
            group_by=False,
            optional=False
        ))
        query_builder.add_element(QElement(
            data='',
            statement='FILTER(?id = \"{start}\")'.format(start=self.start),
            group_by=False,
            optional=False
        ))

    def __copy__(self):
        obj = RootPath(self.start, self.predicates[0])
        for uri in self.predicates[1:]:
            obj.predicates.append(uri)

        return obj


class Element:
    def __init__(self, name, type, value, datatype=None):
        self.name = name
        self.type = type
        self.value = value
        self.datatype = datatype

    def __getitem__(self, item):
        if item is 'name':
            return self.name
        elif item is 'type':
            return self.type
        elif item is 'value':
            return self.value
        elif item is 'datatype':
            return self.datatype


class Endpoint:
    def __init__(self, name=""):
        self.prefix = []
        self.name = name

    def query(self, sparql):
        pass

    def add_prefix(self, prefix, uri):
        self.prefix.append((prefix, uri))

    def childs(self, predicate, obj, path=None):
        query = SelectQuery()

        for prefix, uri in self.prefix:
            query.add_prefix(prefix, uri)
        self.prefix = []

        if path is None:
            query.add_element(
                QElement(data='?p ?o', statement='<{object}> ?p ?o'.format(object=obj), group_by=True, optional=False))
        else:
            path.add_to_query(query)
            query.add_element(QElement(data='?p ?o', statement='?object ?p ?o', group_by=True, optional=False))

        return self.query(query.build())

    def ping(self):
        pass

    def __str__(self):
        return self.name


class GraphEndpoint(Endpoint):

    def __init__(self):
        super().__init__()
        self.graph = Graph()

    def query(self, sparql):
        bindings = []
        logger.info("Query graph endpoint.")
        logger.debug(sparql.replace("\n", " "))
        result_set = self.graph.query(sparql).bindings
        if result_set is None:
            return bindings

        for result in result_set:
            binding = Binding()

            for name in result:
                tmp = type(result[name])
                datatype = None
                if tmp is URIRef:
                    typ = u'uri'
                elif tmp is Literal:
                    typ = u'literal'

                    if result[name].datatype is not None:
                        typ = u'typed-literal'
                        datatype = str(result[name].datatype)
                    if hasattr(result[name], "lang") and result[name].lang is not None:
                        datatype = str(result[name].lang)

                elif tmp is BNode:
                    typ = u'bnode'
                else:
                    typ = u'unknown'

                binding.append(Element(str(name), typ, str(result[name]), datatype=datatype))

            if len(binding.elements) is not 0:
                bindings.append(binding)
        logger.debug("Queried {len} elements.".format(len=len(bindings)))
        return bindings

    def replace_unkown_nodes(self, prefix=""):
        from rdflib import URIRef, BNode, Graph

        ngraph = Graph()
        bnode_map = {}

        def exchange_node(node):
            id = str("{prefix}{node}".format(prefix=prefix, node=node))
            if id not in bnode_map:
                bnode_map[id] = URIRef(id)
            return bnode_map[id]

        for s, p, o in self.graph:
            if type(s) is BNode:
                s = exchange_node(s)
            if type(p) is BNode:
                p = exchange_node(p)
            if type(o) is BNode:
                o = exchange_node(o)
            ngraph.add((s, p, o))

        self.graph = ngraph

    def ping(self):
        try:  # TODO: fix
            with open("demofile.txt", "r") as f:
                return f.readable()
        except IOError:
            pass
        return False


class UrlEndpoint(Endpoint):

    def __init__(self, url):
        super().__init__()
        self.endpoint = SPARQLWrapper(endpoint=url)
        self.endpoint.setReturnFormat(JSON)
        logger.info("Connect to sparql endpoint {url}.".format(url=url))
        self.prefix = []

    def query(self, sparql):
        self.endpoint.setQuery(sparql)
        logger.debug("Query URL endpoint.")
        logger.debug(sparql.replace("\n", " "))
        bindings = []

        for result in self.endpoint.query().convert()['results']['bindings']:
            binding = Binding()
            bindings.append(binding)
            for name in result:
                datatype = None
                if result[name]["type"] == u'typed-literal':
                    datatype = result[name]['datatype']

                binding.append(Element(name, result[name]["type"], result[name]["value"], datatype=datatype))

        logger.debug("Queried {len} elements.".format(len=len(bindings)))
        return bindings

    def ping(self):
        try:
            # urlopen(self.endpoint.endpoint)
            self.endpoint.setQuery("""
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

    SELECT ?o ?p
    WHERE { ?s ?p ?o }
    LIMIT 1
        """)
            self.endpoint.setReturnFormat(JSON)
            results = self.endpoint.query().convert()
            return True
        except:
            return False
