class QElement:

    def __init__(self, data="", statement="", group_by=False, optional=False):
        self.__data = data
        self.__statement = statement
        self.__group_by = group_by
        self.__optional = optional

    def statement(self):
        if self.__optional:
            return "OPTIONAL {{{statement}}}".format(statement=self.__statement)
        return self.__statement

    def data(self):
        return self.__data

    def group_by(self):
        if self.__group_by:
            return self.__data
        return ""

    def optional(self):
        return self.__optional


class QElementsIterator:

    def __init__(self, elements, type):
        self.elements = iter(elements)
        self.type = type
        pass

    def __iter__(self):
        return self

    def __next__(self):
        if self.type == "statement":
            return next(self.elements).statement()
        elif self.type == "data":
            return next(self.elements).data()
        elif self.type == "groupby":
            return next(self.elements).group_by()
        raise StopIteration

    def iter(self):
        return iter(self)


class SelectQuery:
    def __init__(self):
        self.prefixes = []
        self.elements = []
        pass

    def add_prefix(self, prefix, uri):
        self.prefixes.append((prefix, uri))

    def add_element(self, element):
        self.elements.append(element)

    def build_prefix(self):
        view = "PREFIX {prefix}:<{uri}>"
        ret = []
        for prefix in self.prefixes:
            ret.append(view.format(prefix=prefix[0], uri=prefix[1]))

        return "\n".join(ret)

    def build_data(self):
        return " ".join(QElementsIterator(self.elements, "data").iter())

    def build_statement(self):
        return " .\n    ".join(QElementsIterator(self.elements, "statement").iter())

    def build_groupby(self):
        return " ".join(QElementsIterator(self.elements, "groupby").iter())

    def build(self):
        view = """{prefix}
SELECT {data}
WHERE {{
    {statement}
}}
GROUP BY {group_by}"""

        return view.format(prefix=self.build_prefix(), data=self.build_data(), statement=self.build_statement(),
                           group_by=self.build_groupby())

    def __str__(self):
        return self.build()


class ConstructQuery(SelectQuery):

    def build(self):
        view = """{prefix}
CONSTRUCT {{{data}}}
WHERE {{
    {statement}
}}
GROUP BY {group_by}
"""

        return view.format(prefix=self.build_prefix(), data=self.build_data(), statement=self.build_statement(),
                           group_by=self.build_groupby())
