from typing import Optional, List

import rdflib

from app.database.sparql.connector import GraphEndpoint, UrlEndpoint, Endpoint
from app.logic.logger import logger
from app.logic.config import config

cache = {}


def get_endpoints() -> List[Endpoint]:
    endpoints = []
    for conf in config["endpoints"]:
        endpoints.append(connect(conf))

    return endpoints


def get_endpoint(name: str) -> Optional[Endpoint]:
    """

    :param name: str
    :return: Endpoint
    """
    for conf in config["endpoints"]:
        if conf["name"] == name:
            return connect(conf)
    return None


def connect(conf: dict) -> Optional[Endpoint]:
    name = conf["name"]
    type = conf["type"]

    if name in cache:
        return cache[name]

    if type == "url":
        cache[name] = connect_url(conf["url"])
    elif type == "graph":
        replace_bnodes = False
        rdf_format = None

        if "replace_bnodes" in conf:
            replace_bnodes = conf["replace_bnodes"]

        if "rdf_format" in conf:
            rdf_format = conf["rdf_format"]

        cache[name] = connect_graph(conf["file"], replace_bnodes=replace_bnodes, rdf_format=rdf_format)

    if name in cache:
        logger.info("Connect enpoint {name} and add to cache".format(name=name))
        cache[name].name = name
        return cache[name]

    logger.warning("Unable to connect endpoint {name}".format(name=name))
    return None


def connect_url(url: str) -> Endpoint:
    endpoint = UrlEndpoint(url)
    return endpoint


def connect_graph(filename: str, rdf_format: str = None, replace_bnodes: bool = False) -> Endpoint:
    if not rdf_format:
        rdf_format = rdflib.util.guess_format(filename)
        logger.info("No format given. Guessed format {format}.".format(format=rdf_format))

    logger.info("Load Graph from file {file} of type {type} and add to cache".format(file=filename, type=rdf_format))
    endpoint = GraphEndpoint()

    try:
        endpoint.graph.parse(filename, format=rdf_format)
    except Exception as e:
        logger.warning("Loading File failed. Graph will be empty.")
        logger.warning(e)
        replace_bnodes = False

    if replace_bnodes:
        logger.warning("Replace bnodes (should only be used for testing right now)")
        endpoint.replace_unkown_nodes()

    return endpoint
