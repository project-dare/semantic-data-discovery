import time
from queue import Queue, Empty
from threading import Thread

from app.database.solr import solr, ping
from app.logic.config import config
from app.logic.logger import logger

core = config.get("solr", "core")


def list_fields() -> list:
    return solr.schema.get_schema_fields(core)


def field_exists(field: str) -> bool:
    return solr.schema.does_field_exist(core, field)


def create_field(field_dict: dict, copy_field: str = None) -> None:
    logger.info("Create new solr field '{field}'.".format(field=field_dict["name"]))
    solr.schema.create_field(core, field_dict)

    if copy_field is not None and copy_field != field_dict["name"]:
        logger.info("Create copy field from '{field}' to '{copy}'.".format(field=field_dict["name"], copy=copy_field))
        solr.schema.create_copy_field(core, {"source": field_dict["name"], "dest": copy_field})


class FieldCreateWorker(Thread):
    def __init__(self):
        super().__init__()
        self.fields = Queue()
        self.max_retry = 20
        self.duration = 5

    def add_field(self, field_dict, copy_field=None):
        if field_dict["type"] != "_root_":
            self.fields.put((field_dict, copy_field))

        if not self.is_alive():
            self.start()

    def run(self) -> None:
        count = 0
        logger.info("Starting Solr Field Creation Worker.")
        while not ping() and count < self.max_retry:
            logger.info("Waiting for connection to solr...")
            time.sleep(self.duration)
            count += 1

        if count == self.max_retry:
            logger.warning("Was not able to create missing fields due to missing solr connection.")
            return

        try:
            while True:
                field_dict, copy_field = self.fields.get(True, 20)
                if not field_exists(field_dict["name"]):
                    create_field(field_dict, copy_field)
        except Empty as e:
            logger.info("Solr Field Creation Worker finished after no new fields were added.")
            return


creation_worker = FieldCreateWorker()
