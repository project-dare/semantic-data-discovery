import os

from SolrClient import IndexQ, solrresp

from app.database.solr import solr
from app.logic.config import config, ROOT_DIR
from app.logic.logger import logger

temp_path = config.get("solr", "temp_path")

if not os.path.isabs(temp_path):
    temp_path = os.path.join(ROOT_DIR, temp_path)

# TODO: try to reintegrate this or remove it
index = IndexQ(temp_path, "index", size=1, remove_complete=True)


def add(doc: dict) -> None:
    logger.info("Adding {id} to solr index.".format(id=doc["id"]))
    solr.index(config.get("solr", "core"), [doc])


def commit(hard_commit: bool = False) -> None:
    if hard_commit:
        logger.info("Hard commit solr index.")
    else:
        logger.info("Soft commit solr index.")
    solr.commit(config.get("solr", "core"), softCommit=True, openSearcher=hard_commit)


def remove(endpoint: str = None, id: str = None) -> None:
    if id is not None:
        logger.info("Remove solr index with id: {id}.".format(id=id))
        query = "({{!child of=type:root}}+id:\"{id}\") OR id:\"{id}\"".format(id=id)
    elif endpoint is not None:
        logger.info("Remove solr index from endpoint: {endpoint}.".format(endpoint=endpoint))
        query = "({{!child of=type:root}}+endpoint:\"{endpoint}\") OR endpoint:\"{endpoint}\"".format(
            endpoint=endpoint)
    else:
        logger.info("Remove solr index.")
        query = "*:*"
    solr.delete_doc_by_query(config.get("solr", "core"), query)


def show(endpoint: str = None, id: str = None) -> solrresp.SolrResponse:
    if id is not None:
        logger.info("Fetch solr index with id: {id}.".format(id=id))
        query = "type:root AND id:\"{id}\"".format(id=id)
    elif endpoint is not None:
        logger.info("Fetch solr index from endpoint: {endpoint}.".format(endpoint=endpoint))
        query = "type:root AND endpoint:\"{endpoint}\"".format(endpoint=endpoint)
    else:
        logger.info("Fetch solr index.")
        query = "type:root"

    return solr.query(config.get("solr", "core"), {"q": query, "fl": "id,endpoint,_version_", "rows": 9999})
