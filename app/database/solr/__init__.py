from urllib.request import urlopen
import urllib.parse

from SolrClient import SolrClient

from app.logic.config import config
from app.logic.logger import logger

solr = SolrClient(config.get("solr", "host"))
solr.logger = logger
logger.info("Connect to Solr")


def ping() -> bool:
    try:
        urlopen(urllib.parse.urljoin(config.get("solr", "host"), "admin/metrics?type=counter&group=core"))
        return True
    except:
        return False
