from typing import Union

from SolrClient import solrresp

from app.database.solr import solr
from app.logic.config import config


class QueryBuilder:
    def __init__(self):
        self.query = {}

    def __getitem__(self, item):
        return self.query[item]

    def __dict__(self):
        return self.query


class StandardQuery(QueryBuilder):
    def __init__(self, q=""):
        super().__init__()
        self.query = {"q": q}


class DisMaxQuery(QueryBuilder):

    def __init__(self):
        super().__init__()
        self.q = ""
        self.qalt = ""
        self.mm = ""
        self.pf = ""
        self.ps = ""
        self.tie = ""
        self.bp = ""
        self.bf = ""


def query(qobj: Union[dict, QueryBuilder]) -> solrresp.SolrResponse:
    return solr.query(config["solr"]["core"], dict(qobj))
