import getopt
import json
import sys

from app.logic.config import CONFIG_PATH


def help():
    print("Help:")
    print('configurate.py endpoint -n <name> -t [url -u <url>|graph -f <file>]')
    print("or")
    print('configurate.py solr -u <host> -c <core>')
    pass


def main(argv):
    with open(CONFIG_PATH) as f:
        conf = json.load(f)

    if len(argv) < 1 or argv[0] not in ("endpoint", "solr"):
        print("No or wrong command given.\nAvailable commands: entrypoint, solr.\n")
        help()
        sys.exit(7)

    if argv[0] == "endpoint":
        name = ""
        type = ""
        file = ""
        url = ""

        try:
            opts, args = getopt.getopt(argv[1:], "hn:t:fu", ["help", "name=", "type=", "file=", "url="])
        except getopt.GetoptError:
            print("Not enough arguments.\n")
            help()
            sys.exit(2)

        for opt, arg in opts:
            if opt in ("-h", "--help"):
                help()
                sys.exit()
            if opt in ("-n", "--name"):
                name = arg
            if opt in ("-f", "--file"):
                file = arg
            if opt in ("-u", "--url"):
                url = arg
            if opt in ("-t", "--type"):
                if arg not in ("url", "graph"):
                    print("Unknown type.\n")
                    help()
                    sys.exit(3)
                type = arg
        endpoint = {"name": name, "type": type}

        if type == "url" and url == "":
            print("--url needed for type 'url'.\n")
            help()
            sys.exit(4)
        else:
            endpoint["url"] = url

        if type == "graph" and file == "":
            print("--file needed for type 'graph'.\n")
            help()
            sys.exit(5)
        else:
            endpoint["file"] = file
        exists = False

        for id, ep in conf["enpoints"]:
            if ep["name"] == endpoint["name"]:
                exists = True
                conf["enpoints"][id] = endpoint
                print("Endpoint successfully updated.")
        if not exists:
            conf["endpoints"].append(endpoint)
            print("Endpoint successfully added.")

    if argv[0] == "solr":
        host = ""
        core = ""

        try:
            opts, args = getopt.getopt(argv[1:], "hu:c:", ["help", "host=", "core="])
        except getopt.GetoptError:
            print("Not enough arguments.\n")
            help()
            sys.exit(6)

        for opt, arg in opts:
            if opt in ("-h", "--help"):
                help()
                sys.exit()

            if opt in ("-u", "--host"):
                host = arg

            if opt in ("-c", "--core"):
                core = arg

        conf["solr"]["host"] = host
        conf["solr"]["core"] = core
        print("Solr config successfully changed.")

    with open(CONFIG_PATH, 'w') as f:
        json.dump(conf, f, indent=2)

    sys.exit()


if __name__ == "__main__":
    main(sys.argv[1:])
