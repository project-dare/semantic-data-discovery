from werkzeug.contrib.fixers import ProxyFix
import os
from flask import Flask


def create_app(test_config=None):
    # TODO: find a more common solution for this not to loaded locally
    from app.logic.config import config, CONFIG_PATH
    config.load(CONFIG_PATH)

    from app.logic.logger import logger
    from app.ui.apis import api

    # create and configure the app
    app = Flask(__name__)
    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_host=1, x_port=1, x_prefix=1, x_proto=1)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_object(config)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    app.logger = logger

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    api.init_app(app)

    return app
