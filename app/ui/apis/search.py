import time

from flask import request
from flask_restplus import Namespace, Resource, fields as rest_fields

from app.logic.schema.fields import fields as index_fields
from app.logic.search.preview import get_preview_list
from app.ui.apis.utils import check_solr, check_solr_response

ns = Namespace('search', description='Search in indexed datasets')
parser1 = ns.parser()
parser2 = ns.parser()
# parser.add_argument("query", type=str, location='args')
parser1.add_argument("query", type=str, location='args')
parser1.add_argument("start", type=str, location='args')
parser1.add_argument("len", type=str, location='args')
parser1.add_argument("format", type=str, location='args')

parser2.add_argument("query", type=str, location='args')
parser2.add_argument("start", type=str, location='args')
parser2.add_argument("len", type=str, location='args')
parser2.add_argument("format", type=str, location='args')

__import__("app.logic.mapper.vocabularies")
for field in index_fields.values():
    field_type = field.type
    if field_type.queryable:
        parser1.add_argument(field.name, type=field_type.formatter, location='args')
        parser2.add_argument(field.name, type=field_type.formatter, location='form')

result_items = {}
for item in get_preview_list():
    result_items[item] = rest_fields.String()

result = ns.model('Result Item', result_items)


result_set = ns.model('Result', {
    'count': rest_fields.Integer,
    'total': rest_fields.Integer,
    'format': rest_fields.String,
    'start': rest_fields.Integer,
    'len': rest_fields.Integer,
    'time': rest_fields.Float,
    'result': rest_fields.List(rest_fields.Nested(result))})


@ns.route('/')
@ns.doc(responses=check_solr_response())
class Search(Resource):

    @ns.marshal_with(result_set)
    @ns.produces('application/rdf')
    @ns.expect(parser2)
    def post(self):
        self.get()

    @ns.marshal_with(result_set)
    @ns.produces('application/rdf')
    @ns.expect(parser1)
    @ns.doc(id='search-get', summary="Search in index",
            description="Offers a full-text search query and filter fields to search in the index.")
    def get(self):
        check_solr()
        from app.logic.search.perform import search

        result = {}
        start = 0
        length = 10
        format = "turtle"
        query = None
        params = dict(request.args)

        if "start" in request.args:
            start = int(request.args["start"])
            del (params["start"])

        if "len" in request.args:
            length = int(request.args["len"])
            del (params["len"])

        if "format" in request.args:
            format = request.args["format"]
            del (params["format"])

        if "query" in request.args:
            query = request.args["query"]
            del (params["query"])

        result["result"] = []

        start_time = int(round(time.time() * 1000))
        result["total"], result["result"] = search(params, search_query=query, start=start, length=length,
                                                   format=format)
        end_time = int(round(time.time() * 1000))

        result["time"] = end_time - start_time
        result["format"] = format
        result["start"] = start
        result["len"] = length
        result["count"] = len(result["result"])
        return result
