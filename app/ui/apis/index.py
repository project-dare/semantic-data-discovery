import json

from flask import Response, request
from flask_restplus import Namespace, Resource, fields

from app.logic.config import config
from app.ui.apis.utils import check_solr, check_endpoint, check_solr_response, check_endpoint_response, combine_response

ns = Namespace('index', description='Manage search index (view, create, delete)')
parser = ns.parser()
parser.add_argument('dataset', type=str, required=False)

dataset_model = ns.model('Dataset', {
    'id': fields.String,
    'endpoint': fields.String,
    'version': fields.String(attribute='_version_')})


@ns.route('/')
@ns.doc(responses=check_solr_response())
class Index(Resource):

    @ns.produces('application/json')
    @ns.marshal_with(dataset_model)
    @ns.doc(id='index-get', summary="Indexed datasets",
            description="Lists all indexed datasets with source endpoint and unique URI.")
    def get(self):
        check_solr()

        from app.logic.mapper.indexer import index_show
        return index_show()

    @ns.produces('application/json')
    @ns.doc(id='index-put', summary="Index all",
            description="Create/Updates index from all datasets found in the configured endpoints.")
    def put(self):
        check_solr()

        from app.logic.mapper.indexer import index_all
        res = index_all()
        return Response(json.dumps(res), mimetype='application/json')

    @ns.produces('application/json')
    @ns.doc(id='index-delete', summary="Delete all",
            description="Deletes the index for all datasets. Usefull to reindex everything after a schema change.")
    def delete(self):
        check_solr()

        from app.logic.mapper.indexer import index_remove
        index_remove()
        return Response(json.dumps({"status": "ok"}), mimetype='application/json')


@ns.route('/endpoints/<string:name>')
@ns.doc(responses=combine_response(check_solr_response(), check_endpoint_response()))
class IndexEndpoint(Resource):

    @ns.produces('application/json')
    @ns.marshal_with(dataset_model)
    @ns.expect(parser)
    @ns.doc(id='index_endpoint_name-get', summary="Indexed datasets on a endpoint",
            description="Lists all indexed datasets on an specified endpoint with source endpoint and unique URI.")
    def get(self, name):
        check_solr()
        from app.logic.mapper.indexer import index_show
        check_endpoint(name)

        dataset = None
        if "dataset" in request.args:
            dataset = request.args["dataset"]

        return index_show(name, dataset)

    @ns.produces('application/json')
    @ns.expect(parser)
    @ns.doc(id='index_endpoint_name-put', summary="Index endpoint",
            description="Create/Updates index from all datasets found in an specified endpoints. "
                        "Single dataset only is possible.")
    def put(self, name):
        check_solr()
        from app.logic.mapper.indexer import index_dataset, index_endpoint
        check_endpoint(name)

        if "dataset" in request.args:
            ok = index_dataset(name, request.args["dataset"], hard_commit=True)
            if ok:
                res = {"status": "ok"}
            else:
                res = {"status": "not ok"}
        else:
            res = index_endpoint(name, hard_commit=True)
        return Response(json.dumps(res), mimetype='application/json')

    @ns.produces('application/json')
    @ns.expect(parser)
    @ns.doc(id='index_endpoint_name-delete', summary="Delete index of an endpoint",
            description="Deletes the index for all datasets from an specified endpoint. "
                        "Single dataset only is possible.")
    def delete(self, name):
        check_solr()
        from app.logic.mapper.indexer import index_remove
        check_endpoint(name)

        dataset = None
        if "dataset" in request.args:
            dataset = request.args["dataset"]

        if index_remove(name, dataset):
            res = {"status": "ok"}
        else:
            res = {"status": "not ok"}

        return Response(json.dumps(res), mimetype='application/json')


@ns.route('/endpoints/')
class Endpoint(Resource):

    @ns.produces('application/json')
    @ns.doc("Shows configured endpoints", id='index_endpoint-get', summary="Shows configured endpoints",
            description="Lists the exact name of all endpoints configured in this application.")
    def get(self):
        resp = []
        for conf in config["endpoints"]:
            resp.append(conf["name"])

        return Response(json.dumps(resp), mimetype='application/json')
