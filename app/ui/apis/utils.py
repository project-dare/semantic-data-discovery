from http import HTTPStatus

from flask_restplus import abort

from app.logic.utils import endpoint_exists, endpoint_connected, solr_connected


def check_endpoint_response():
    return {HTTPStatus.BAD_REQUEST: 'Endpoint not found', HTTPStatus.BAD_GATEWAY: "Can't connect to endpoint {name}."}


def check_endpoint(name):
    if not endpoint_exists(name):
        abort(HTTPStatus.BAD_REQUEST, 'Endpoint not found')

    if not endpoint_connected(name):
        abort(HTTPStatus.BAD_GATEWAY, "Can't connect to endpoint {name}.".format(name=name))


def check_solr_response():
    return {HTTPStatus.BAD_GATEWAY: "Can't connect to solr"}


def check_solr():
    if not solr_connected():
        abort(HTTPStatus.BAD_GATEWAY, "Can't connect to solr")


def combine_response(*responses):
    response = {}

    for resp in responses:
        for id in resp:
            if id in response:
                response[id] = "{a}\n{b}".format(a=response[id], b=resp[id])
            else:
                response[id] = resp[id]

    return response
