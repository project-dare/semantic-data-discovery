from flask import make_response
from flask_restplus import Api

from app.logic.config import config
from app.ui.apis.search import ns as ns_search
from app.ui.apis.index import ns as ns_index
from app.ui.apis.monitoring import ns as ns_monitoring

api = Api(
    title=config["app"]["name"],
    version='1.0',
    description=config["app"]["description"],
    doc="/api/"
)


@api.representation('text/plain')
def text(data, code, headers):
    headers['mediatype'] = 'text/plain'
    resp = make_response(str(data), code)
    resp.headers.extend(headers)


@api.representation('application/rdf')
def text(data, code, headers):
    headers['mediatype'] = 'application/rdf'
    resp = make_response(str(data), code)
    resp.headers.extend(headers)


api.add_namespace(ns_search, path='/search')
api.add_namespace(ns_index, path='/index')
api.add_namespace(ns_monitoring, path='/monitor')
