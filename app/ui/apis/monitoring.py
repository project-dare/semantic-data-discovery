from flask_restplus import Namespace, Resource
from flask import request

from app.logic.monitor import ping_all

ns = Namespace('monitoring', description='Monitor application')


@ns.route('/status')
class Status(Resource):

    @ns.produces('application/json')
    @ns.doc(id='monitor_status-get', summary="Lookup application",
            description="Checks the connection to the external services solr and endpoints.")
    def get(self):
        return ping_all()


@ns.route('/heartbeat')
class Status(Resource):

    @ns.produces('application/json')
    @ns.doc(id='monitor_heartbeat-get', summary="Heart Beat",
            description="Offers an easy-to-call resource to test the connection to Semantic Data Discovery.")
    def get(self):
        return {"status": "ok"}

@ns.route('/proxy_header')
@ns.hide
class Header(Resource):

    @ns.produces('application/json')
    def get(self):
        return dict(request.headers)
