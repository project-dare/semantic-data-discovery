from app.database import solr
from app.database.sparql.endpoint import get_endpoint
from app.logic.config import config


def endpoint_exists(name: str) -> bool:
    for endpoint in config["endpoints"]:
        if endpoint["name"] == name:
            return True
    return False


def endpoint_connected(name: str) -> bool:
    endpoint = get_endpoint(name)
    if endpoint is not None:
        return endpoint.ping()
    return False


def solr_connected() -> bool:
    return solr.ping()
