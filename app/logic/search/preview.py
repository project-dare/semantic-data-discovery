from app.logic.config import config
from app.logic.search.content import build as build_contet

preview_system_default = ["id", "endpoint", "dct_identifier"]


def get_preview_list() -> list:
    """
    Retruns all preview fields configured in the config.json.
    :return:list
    """
    preview_fields = ["id"]

    if "search_preview" in config["app"]:
        for field in config["app"]["search_preview"]:
            preview_fields.append(field)
    else:
        preview_fields = preview_system_default

    return preview_fields


def build_preview_list() -> str:
    """
    Builds the comma separated preview list for solr
    :return: str
    """
    preview_list = list(preview_system_default)
    preview_list.extend(x for x in get_preview_list() if x not in preview_list)

    return ",".join(preview_list)


def build_preview(raw: dict, content_format: str, custom_preview_list: list = None) -> dict:
    """
    Builds the dict which contains all information's requested.
    :param raw: dict
    :param content_format: str
    :param custom_preview_list: list
    :return: dict
    """
    endpoint = raw["endpoint"]
    id = raw["id"]
    preview = {"id": id}
    if custom_preview_list is not None:
        preview_list = custom_preview_list
    else:
        preview_list = get_preview_list()

    for field in preview_list:
        if field == "content":
            if "dct_identifier" in raw:
                identifier = raw["dct_identifier"]
                if type(identifier) is list:
                    identifier = identifier[0]
            else:
                identifier = id
            data = build_contet(endpoint, id, format=content_format, ident=identifier)
        # elif field == "something":
        elif field in raw:
            data = raw[field]
        else:
            data = ""

        if type(data) is list:
            data = "\n".join(data)
        preview[field] = data

    return preview
