from app.database.solr import query as solr
from app.logic.logger import logger
from app.logic.search.preview import build_preview, build_preview_list


def search(params: dict, search_query: str = None, start: int = 0, length: int = 10, format: str = "turtle") -> (int, list):
    """
    Performs the search call on solr
    :param params: dict
    :param search_query: str
    :param start: int
    :param length: int
    :param format: str
    :return: (int, list)
    """
    if search_query is None:
        search_query = "*"

    logger.info("Search with query: {query}".format(query=search_query))
    res = solr.query({
        'q': search_query,
        'fq': build_filter(params),
        "fl": build_preview_list(),
        "rows": length,
        "start": start,
        "defType": "edismax",
        "qf": "text"
    })
    ret = []

    for doc in res.docs:
        ret.append(build_preview(doc, format))
    return res.get_num_found(), ret


def build_filter(params: dict, op: str = "AND", ) -> str:
    """
    Builds the filter str by converting the given parameter
    :param params: dict
    :param op: str
    :return: str
    """
    query_parts = []
    for field in params:
        query_parts.append("{field}:\"{search}\"".format(field=field, search=params[field]))
    op = " {op} ".format(op=op)

    if len(query_parts) > 0:
        fil = "({{!parent which='type:root'}}+{parts}) OR {parts}".format(parts=op.join(query_parts))
    else:
        fil = "{!parent which='type:root'}"

    logger.info("Search with filter: {filter}".format(filter=fil))
    return fil
