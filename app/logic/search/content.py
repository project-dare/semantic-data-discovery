from rdflib import URIRef, BNode, Literal, Graph

from app.database.sparql.connector import Path, RootPath
from app.database.sparql.endpoint import get_endpoint
from app.database.sparql.query_builder import QElement, SelectQuery


def build(endpoint_name, uri, format="turtle", ident=""):
    endpoint = get_endpoint(endpoint_name)
    graph = Graph()
    path = None
    if uri.startswith("nodeID://") and ident != "":
        path = RootPath(ident, "http://purl.org/dc/terms/identifier")
    rec(endpoint, graph, uri, path)

    return graph.serialize(format=format).decode("utf-8")


def rec(endpoint, graph, uri, path=None):
    query = SelectQuery()
    if path is not None:
        path.add_to_query(query)
        query.add_element(
            QElement(data='?p ?o', statement='?object ?p ?o', group_by=True, optional=False))
        s = BNode(uri)
    else:
        query.add_element(
            QElement(data='?p ?o', statement='<{id}> ?p ?o'.format(id=uri), group_by=True, optional=False))
        s = URIRef(uri)

    result_set = endpoint.query(query.build())

    for result in result_set:
        p = conv_rdflib_type(result["p"])
        o = conv_rdflib_type(result["o"])
        graph.add((s, p, o))
        if result["o"]["type"] == "uri":
            rec(endpoint, graph, result["o"]["value"])
        if result["o"]["type"] == "bnode":
            if path is None:
                path = Path(uri, result["p"]["value"])
            else:
                path.predicates.append(result["p"]["value"])


def conv_rdflib_type(data):
    type = data["type"]
    value = data["value"]
    if type == "literal":
        return Literal(value)
    if type == "typed-literal":
        literal = Literal(value, datatype=data["datatype"])
        return literal
    elif type == "uri":
        return URIRef(value)
    elif type == "bnode":
        return BNode(value)
