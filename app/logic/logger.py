import logging

from app.logic.config import config

if "app" in config and "log_level" in config["app"]:
    level = config["app"]["log_level"]
else:
    level = "DEBUG"

logger = logging.getLogger()
logger.setLevel(level)
# logger.setLevel(logging.getLevelName(config["app"]["log_level_console"]))
formatter = logging.Formatter('%(asctime)s[%(name)s][%(levelname)s]: %(message)s')

cmd = logging.StreamHandler()
cmd.setFormatter(formatter)
logger.addHandler(cmd)

if "app" in config and "log_file" in config["app"]:
    file = logging.FileHandler(config["app"]["log_file"])
    file.setFormatter(formatter)
    logger.addHandler(file)

