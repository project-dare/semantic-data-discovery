import re

from app.database.sparql.connector import Path
from app.logic.logger import logger
from app.logic.schema.fields import add_field

vocabularies = {}
uri_cache = {}


def vocabulary():
    def wrapper(cls):
        if issubclass(cls, Vocabulary):
            obj = cls()
            vocabularies[obj.uri] = obj
            logger.info("Add Vocabulary {uri}.".format(uri=obj.uri))
        return cls

    return wrapper


def add_vocabulary(obj):
    if issubclass(type(obj), Vocabulary):
        vocabularies[obj.uri] = obj
        logger.info("Add Vocabulary {uri}.".format(uri=obj.uri))


def find_vocabulary(uri):
    if uri in uri_cache:
        return uri_cache[uri]
    vocabularies.keys()
    for voc in vocabularies.keys():
        if voc in uri:
            uri_cache[uri] = voc
            return voc

    return ""


def resolve(request, ret, rdf_type="uri"):
    uri = find_vocabulary(request.predicate)

    if uri != "":
        vocabularies[uri]._resolve(request, ret, rdf_type)
    else:
        logger.info("Resolve: Vocabulary <{uri}> not found".format(uri=request.predicate))


def literal(request, ret):
    uri = find_vocabulary(request.predicate)

    if uri != "":
        vocabularies[uri]._literal(request, ret)
        return

    logger.info("Resolve: Vocabulary <{uri}> not found".format(uri=request.predicate))


def shorten(uri):
    for voc_uri in vocabularies.keys():
        if voc_uri in uri:
            voc = vocabularies[voc_uri]
            return voc._shorten(uri)

    return uri


def atr_flat(name, result, obj):
    if type(obj) is dict:
        if "id" in obj:
            atr_simple(name, result, obj["id"])
            del (obj["id"])
        if "type" in obj:
            del (obj["type"])

        for id in obj:
            atr_simple(id, result, obj[id])
    else:
        atr_simple(name, result, obj)


def atr_simple(name, result, obj):
    if name in result:
        if type(result[name]) is not list:
            result[name] = [result[name]]
        result[name].append(obj)
    else:
        result[name] = obj


document_strategy = atr_simple


def add_to_result(name, result, obj):
    document_strategy(name, result, obj)


def build_short(prefix, suffix):
    return "{prefix}_{suffix}".format(prefix=prefix, suffix=suffix)


class Request:

    def __init__(self, predicate, obj, endpoint, path=None, literal_type=None):
        """
        :param predicate: string
        :param obj: string
        :param endpoint: app.database.sparql.connector.Endpoint
        """
        self.predicate = predicate
        self.object = obj
        self.endpoint = endpoint
        self.path = path
        self.literal_type = literal_type


class Vocabulary:
    discovered = []

    def __init__(self, uri, prefix):
        self.uri = uri
        self.prefix = prefix
        self.indexable = {}
        self.suffix_cache = {}

    def add_indexable(self, field, type, default="", properties=None):
        if properties is None:
            properties = {}
        fieldname = build_short(self.prefix, field)
        self.indexable[field] = add_field(fieldname, type, default, properties)

    def _suffix(self, uri):
        if uri in self.suffix_cache:
            return self.suffix_cache[uri]

        regex = re.compile('[^a-zA-Z]')
        suffix = regex.sub('', uri.replace(self.uri, '')).lower()
        self.suffix_cache[uri] = suffix
        return suffix

    def _build_uri(self, name):
        return "{uri}{name}".format(uri=self.uri, name=name)

    def _shorten(self, uri):
        return build_short(self.prefix, self._suffix(uri))

    def _literal(self, request, ret):
        field_name = self._suffix(request.predicate)

        if self._has_custom(field_name):
            res = {}
            self._call_custom(field_name, request, "literal", res)
            if "" in res and len(res) > 1:
                res[self._shorten(request.predicate)] = res[""]
                del(res[""])
            elif "" in res:
                add_to_result(self._shorten(request.predicate), ret, res[""])
            elif len(res) > 1:
                add_to_result(self._shorten(request.predicate), ret, res)

        elif field_name in self.indexable:
            data = self.indexable[field_name].type.formatter(request.object)

            add_to_result(self._shorten(request.predicate), ret, data)
        else:
            logger.debug("Ignored Field {predicate} - {obj}".format(predicate=request.predicate, obj=request.object))

    def _resolve(self, request, ret, rdf_type="uri"):
        field_name = self._suffix(request.predicate)
        res = {}

        if self._has_custom(field_name):
            self._call_custom(field_name, request, rdf_type, res)
        elif field_name in self.indexable:
            self._discover(request, res)
        else:
            logger.debug("Ignored uri {predicate} - {obj}".format(predicate=request.predicate, obj=request.object))

        if res is not None and len(res) > 0:
            if type(res) is dict:
                ident = str(request.object)
                if rdf_type == "bnode" and not ident.startswith("nodeID://"):
                    ident = "nodeID://{id}".format(id=ident)
                res["id"] = ident
                res["type"] = self._shorten(request.predicate)
            add_to_result(self._shorten(request.predicate), ret, res)
    
    def _has_custom(self, field_name):
        return hasattr(self, field_name)
    
    def _call_custom(self, field_name, request, type, res):
        simple_res = getattr(self, field_name)(request, type, res)
        if simple_res is not None:
            res[""] = simple_res

    def _discover(self, request, ret, recursive=True, white_list=None, verbose=False):
        if white_list is None:
            white_list = []
        self.discovered.append(request.object)
        if request.path is not None:
            logger.debug("Discover node with path: {path}".format(path=request.path))
        result_set = request.endpoint.childs(request.predicate, request.object, path=request.path)

        for result in result_set:
            sub_predicate = result['p']['value']
            sub_obj = result['o']['value']
            short = shorten(sub_predicate)

            if len(white_list) > 0 and short not in white_list:
                continue

            sub_request = Request(sub_predicate, sub_obj, request.endpoint)

            if result['o']['type'] == u'uri' and recursive and sub_obj not in self.discovered:
                if verbose:
                    logger.debug("Resolve: Uri of type <{short}> with name <{name}>"
                                 .format(short=short, name={sub_obj}))
                resolve(sub_request, ret)

            if result['o']['type'] == u'bnode' and recursive and sub_obj not in self.discovered:
                if verbose:
                    logger.debug("Resolve: Blank Node of type <{short}>.".format(short=short))
                if request.path is None:
                    path = Path(request.object, sub_request.predicate)
                else:
                    path = request.path.__copy__()
                    path.predicates.append(sub_request.predicate)

                sub_request.path = path
                resolve(sub_request, ret, rdf_type="bnode")

            elif result['o']['type'] == u'literal':
                if verbose:
                    logger.debug("Resolve: Literal of type <{short}> with value <{value}>"
                                 .format(short=short, value={sub_obj}))
                literal(sub_request, ret)

            elif result['o']['type'] == u'typed-literal':
                if verbose:
                    logger.debug("Resolve: Typed Literal of type <{short}> with value <{value}>"
                                 .format(short=short, value={sub_obj}))
                sub_request.literal_type = result['o']['datatype']
                literal(sub_request, ret)
