def geometry(self, request, type, res):
    if request.literal_type == "http://www.openlinksw.com/schemas/virtrdf#Geometry" \
            or request.literal_type == "https://www.iana.org/assignments/media-types/application/vnd.geo+json":
        return request.object
