import importlib
import os
import xml.etree.ElementTree as ET

from app.logic import schema
from app.logic.config import config
from app.logic.logger import logger
from app.logic.mapper.linked import Vocabulary, add_vocabulary


class Generic(Vocabulary):

    def __init__(self, vocabulary_info):
        super().__init__(vocabulary_info["uri"], vocabulary_info["prefix"])
        self.customs = {}
        for field in vocabulary_info["fields"]:
            field_type = getattr(schema, field["type"])
            self.add_indexable(field["name"], field_type, default=field["default"], properties=field["properties"])

            if "custom" in field:
                self.customs[field["name"]] = self._get_module(field["custom"])

    def _get_module(self, name):
        parts = name.rsplit('.', 1)
        try:
            module = importlib.import_module(parts[0])
        except ModuleNotFoundError:
            logger.warning("Path '{path}' could not be found.".format(path=name))
            return None
        func = getattr(module, parts[1])
        if not callable(func):
            logger.warning("Path '{path}' is not callable.".format(path=name))
            return None

        return func

    def _has_custom(self, field_name):
        return field_name in self.customs

    def _call_custom(self, field_name, request, type, res):
        func = self.customs[field_name]
        simple_res = func(self, request, type, res)
        if simple_res is not None:
            res[""] = simple_res


def parse_xml(file_name):
    if not os.path.isfile(file_name):
        return False
    xml_file = ET.parse(file_name)
    xml_vocs = xml_file.getroot()
    for xml_voc in xml_vocs:
        vocabular = {"uri": "", "prefix": ""}

        for val in xml_voc:
            if val.tag == "uri":
                vocabular["uri"] = val.text
            if val.tag == "prefix":
                vocabular["prefix"] = val.text
            if val.tag == "fields":
                fields = []
                for xml_field in val:
                    field = {"name": "", "type": "", "default": ""}
                    for id in ("name", "type", "custom", "default"):
                        if id in xml_field.attrib:
                            field[id] = xml_field.attrib[id]
                    properties = {}
                    for xml_property in xml_field:
                        properties[xml_property.attrib["name"]] = \
                            True if xml_property.attrib["value"].lower() == "true" else False
                    field["properties"] = properties
                    fields.append(field)
                vocabular["fields"] = fields
        if vocabular["uri"] == "" or vocabular["prefix"] == "":
            return False
        add_vocabulary(Generic(vocabular))

    return True


if type(config["app"]["mapping_file"]) is list:
    for file_name in config["app"]["mapping_file"]:
        parse_xml(file_name)
else:
    parse_xml(config["app"]["mapping_file"])
