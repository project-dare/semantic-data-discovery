def spatial(self, request, type, res):
    if type == "uri" or type == "bnode":
        self._discover(request, res, verbose=True)
    if type == "literal":
        from app.logic.schema.fields import fields
        if "locn_geometry" in fields:
            res["locn_geometry"] = request.object


def identifier(self, request, type, res):
    if type == "literal":
        return request.object
