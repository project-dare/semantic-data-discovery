from app.logic.mapper.linked import vocabulary, Vocabulary
from app.logic.schema import TEXT, CONTAINER, ROOT


@vocabulary()
class Dcat(Vocabulary):
    def __init__(self):
        super().__init__("http://www.w3.org/ns/dcat#", "dcat")
        self.add_indexable("keyword", TEXT)
        self.add_indexable("mediatype", TEXT)
        self.add_indexable("distribution", CONTAINER)
        self.add_indexable("dataset", ROOT)

    def distribution(self, request, type, res):
        if type == "uri" or type == "bnode":
            self._discover(request, res)

    def mediatype(self, request, type, res):
        if type == "uri":
            base_uri = "https://www.iana.org/assignments/media-types/"
            return request.object.replace(base_uri, '')
        if type == "literal":
            return request.object

    def dataset(self, request, type, res):
        if type == "uri" or type == "bnode":
            self._discover(request, res)


@vocabulary()
class Adms(Vocabulary):
    def __init__(self):
        super().__init__("http://www.w3.org/ns/adms#", "adms")


@vocabulary()
class Foaf(Vocabulary):
    def __init__(self):
        super().__init__("http://xmlns.com/foaf/0.1/", "foaf")


@vocabulary()
class Rdf(Vocabulary):
    def __init__(self):
        super().__init__("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf")


@vocabulary()
class Rdf(Vocabulary):
    def __init__(self):
        super().__init__("http://www.w3.org/2000/01/rdf-schema#", "rdfs")
