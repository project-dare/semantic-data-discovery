from typing import Union, Optional, List

from app.database.solr import index
from app.database.sparql.connector import Endpoint, RootPath, Path
from app.database.sparql.endpoint import get_endpoints, get_endpoint
from app.database.sparql.query_builder import SelectQuery, QElement

from app.logic.logger import logger
from app.logic.mapper.linked import resolve, Request, document_strategy, atr_flat

query = SelectQuery()
query.add_prefix("dcat", "http://www.w3.org/ns/dcat#")
query.add_prefix("dc", "http://purl.org/dc/terms/")
query.add_element(
    QElement(data='?dataset', statement='?source dcat:dataset ?dataset', group_by=True, optional=False))
query.add_element(
    QElement(data='?id', statement='?dataset dc:identifier ?id', group_by=True, optional=True))


def index_all() -> dict:
    logger.info("Start index all")
    res = {}
    for endpoint in get_endpoints():
        res[endpoint.name] = index_endpoint(endpoint)
    index.commit(True)
    logger.info("End index all")
    return res


def index_endpoint(endpoint: Union[str, Endpoint], size: int = -1, hard_commit: bool = False) -> dict:
    if not isinstance(endpoint, Endpoint):
        endpoint = get_endpoint(endpoint)
        if endpoint is None:
            logger.warning("Endpoint not found.".format(name=endpoint))
            return {"status": "not found"}
    i = 0
    f = 0

    logger.info("Start index endpoint {name}".format(name=endpoint.name))
    result_set = endpoint.query(query.build())

    for result in result_set:
        if i is size:
            break
        if result["dataset"]["type"] == "bnode":
            path = RootPath(result["id"]["value"], "http://purl.org/dc/terms/identifier")
            success = index_dataset(endpoint, result["dataset"]["value"], path=path)
            pass
        else:
            success = index_dataset(endpoint, result["dataset"]["value"])
        if not success:
            f = f + 1

        i = i + 1
    index.commit(hard_commit)
    logger.info("End index endpoint {name}".format(name=endpoint.name))

    return {"status": "ok", "datasets": i, "failed": f}


def index_dataset(endpoint: Union[str, Endpoint], uri: str, hard_commit: bool = False, path: Path = None) -> bool:
    if not isinstance(endpoint, Endpoint):
        endpoint = get_endpoint(endpoint)
        if endpoint is None:
            logger.warning("Endpoint not found.".format(name=endpoint))
            return False
    success = False
    res = {}
    logger.info("Resolve dataset <{uri}>".format(uri=uri))
    rdf_type = "uri" if path is None else "bnode"

    resolve(Request(u"http://www.w3.org/ns/dcat#dataset", uri, endpoint, path=path), res, rdf_type)
    if type(res) is dict and len(res) > 0:
        doc = {}
        if document_strategy is atr_flat:
            doc = res
            ident = doc["dcat_dataset"]
            if path is not None and not ident.startswith("nodeID://"):
                ident = "nodeID://{id}".format(id=ident)
            doc["id"] = ident
            del (doc["dcat_dataset"])
        else:
            doc = res["dcat_dataset"]

        doc["endpoint"] = endpoint.name
        doc["type"] = "root"
        try:
            index.add(doc)
            success = True
        except Exception as e:
            logger.warning("Error indexing dataset <{uri}>.".format(uri=uri))
            logger.warning(e)
    else:
        logger.info("Dataset <{uri}> lead to no result.".format(uri=uri))

    if hard_commit:
        index.commit(True)
    return success


def index_remove(endpoint: Optional[str] = None, id: Optional[str] = None) -> None:
    index.remove(endpoint, id)
    index.commit(True)


def index_show(endpoint: Optional[str] = None, id: Optional[str] = None) -> List[str]:
    ret = index.show(endpoint, id)
    return ret.docs
