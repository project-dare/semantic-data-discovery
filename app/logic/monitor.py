from app.database.sparql.endpoint import get_endpoints
from app.database import solr


def ping_all() -> dict:
    endpoint_pings = {}
    for endpoint in get_endpoints():
        endpoint_pings[endpoint.name] = endpoint.ping()

    solr_ping = solr.ping()

    return {"solr": solr_ping, "endpoints": endpoint_pings}
