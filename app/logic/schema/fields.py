from app.database.solr.schema import creation_worker
from app.logic.logger import logger

fields = {}


class FieldType:

    def __init__(self, name, sclass, properties=None, formatter=None, queryable=True, copy_field=None):
        if properties is None:
            properties = {}
        if formatter is None:
            formatter = str
        self.name = name
        self.sclass = sclass
        self.properties = properties
        self.formatter = formatter
        self.queryable = queryable
        self.copy = copy_field

    def add_property(self, name, value):
        self.properties[name] = bool(value)


class Field:

    def __init__(self, name, type, default="", properties=None):
        if properties is None:
            properties = {}
        self.name = name
        self.type = type
        self.default = default
        self.properties = properties

    def add_property(self, name, value):
        self.properties[name] = bool(value)

    def __iter__(self):
        yield 'name', self.name
        yield 'type', self.type.name
        if self.default is not "":
            yield 'default', self.default
        for key in self.properties:
            yield key, self.properties[key]


def pdate(x):
    return x


def location(x):
    return x


def add_field(name: str, type: FieldType, default: str = "", properties: dict = None) -> Field:
    if properties is None:
        properties = {}
    field = Field(name, type, default=default, properties=properties)
    fields[name] = field

    creation_worker.add_field(dict(fields[name]), type.copy)

    logger.debug("Add {field} to known solr fields".format(field=name))
    return field
