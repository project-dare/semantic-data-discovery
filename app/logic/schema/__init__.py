from flask_restplus import inputs

from app.logic.schema.fields import FieldType, add_field, location, pdate

CONTAINER = FieldType("_nest_path_", "solr.NestPathField", queryable=False, properties={"multiValued": True})
ROOT = FieldType("_root_", "", queryable=False, properties={"stored": True})
BOOLEAN = FieldType("boolean", "solr.BoolField", formatter=inputs.boolean)  #
# f_rdate = FieldType("rdate", "solr.DateRangeField")
DATE = FieldType("pdate", "solr.DatePointField", formatter=pdate)
# f_enum = FieldType("enum", "solr.EnumFieldType")
FLOAT = FieldType("pfloat", "solr.FloatPointField", formatter=float)
INT = FieldType("pint", "solr.IntPointField", formatter=int)
LOCATION_POINT = FieldType("location", "solr.LatLonPointSpatialField", formatter=location)
LOCATION_POLYGON = FieldType("location_rpt", "solr.SpatialRecursivePrefixTreeFieldType", formatter=location)
LOCATION = LOCATION_POINT
LONG = FieldType("plong", "solr.LongPointField", formatter=int)
TEXT = FieldType("text_general", "solr.TextField", formatter=str, copy_field="text")
META = FieldType("text_gen_sort", "solr.TextField", formatter=str)

# Local fields
add_field("id", TEXT, properties={"multiValued": False, "stored": True, "docValues": True})
add_field("endpoint", META, properties={"multiValued": False, "stored": True, "docValues": True})
add_field("type", META, properties={"multiValued": False, "stored": True, "docValues": True})
# copy field for all text inputs
add_field("text", TEXT, properties={"indexed": True, "stored": False, "multiValued": True})
