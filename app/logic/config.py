import json
import os

from app.logic import ConfigNotFoundError

ROOT_DIR = os.getcwd()
CONFIG_PATH = os.path.join(ROOT_DIR, './config/config.json')

if not os.path.isfile(CONFIG_PATH):
    CONFIG_PATH = '/etc/app/config.json'


class Config:
    def __init__(self):
        self.conf = {}

    def load(self, path):
        if os.path.isfile(path):
            with open(path) as f:
                data = json.load(f)

            self.conf.update(data)
        else:
            # is called before logger so no error logging is available
            raise ConfigNotFoundError("Was not able to find config file {file}.".format(file=path))

    def get(self, module, variable=""):

        if module in self.conf:
            if variable is "":
                return self.conf[module]
            elif variable in self.conf[module]:
                return self.conf[module][variable]
        return ""

    def __getitem__(self, item):
        return self.conf[item]

    def __iter__(self):
        return self.conf.__iter__()

    def __next__(self):
        return next(self.conf)


config = Config()
