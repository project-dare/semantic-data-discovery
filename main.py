#! /bin/python
from app.logic.logger import logger
from app.ui import create_app

logger.info("Start")
flask = create_app()
flask.run(debug=True)
logger.info("Stop")
